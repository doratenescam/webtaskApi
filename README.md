## Technologies
***
Technologies used within the project:
* [mongodb](https://www.mongodb.com/cloud/atlas): MondoDB-- Cloud database service.
* [nodejs](https://nodejs.org/en/): Version 14.15.4
* [express](https://www.npmjs.com/package/express): Version 4.17.1
* [mongoose](https://www.npmjs.com/package/mongoose): Version 5.12.3


## Installation
***
* git clone https://gitlab.com/doratenescam/webtaskApi.git
* cd ../path/to/the/file
* npm install
* In case to connect to other DB: Config the variable uri at config/db.js and use the file car.js
  where there info to import at a mongoDB.
  ***
  The DB configured currently is working on the cloud, 
   so it will be available for the revision.
   
* node server.js

If the API and DB conecction are running ok, you should see:

> Server running at http://localhost:3000
> Database connection ok!

## APIs availables fot test
***
- Cars list: http://localhost:3000/cars
- Cars detail info: http://localhost:3000/cars/id


