'use strict'

// Import DB Connection
require("./config/db");

// require express and bodyParser
const  express = require("express");
const  bodyParser = require("body-parser");
const  port = process.env.PORT || 3000;
var cors = require('cors');
// create express app
const  app = express();

// use bodyParser middleware on express app
app.use(cors());
app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

// Import API route
var routes = require('./api/routes/carRoutes'); //importing route
routes(app);

// Add endpoint
app.get('/', (req, res) => {
    res.send("Hello World");
});

// Listen to server
app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`);
});