'use strict';
// Import mongoose
const mongoose = require("mongoose");

// Declare schema and assign Schema class
const Schema = mongoose.Schema;


var CarSchema = new Schema({
  name: {
    type: String
  },
  year: {
    type: Number
  },
  priceRange: {
    type: Object
  },
  mileage: {
    type: Number
  },
  itemNumber: {
    type: String
  },
  vin: {
    type: String
  },
  views: {
    type: Number
  },
  saves: {
    type: Number
  },
  shares: {
    type: Number
  },
  exterior: {
    type: Object
  },
  performance: {
    type: Object
  },
  images: {
    type: Array
  },
  thumbnailImages: {
    type: Array
  },
});

module.exports = mongoose.model("Cars", CarSchema);