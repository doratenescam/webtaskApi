// import Todo Model
const  Car = require("../models/carModel");

// DEFINE CONTROLLER FUNCTIONS

// List all cars
exports.listAllCars = (req, res) => {
  Car.find({}, (err, list) => {
  if (err) {
    res.status(500).send(err);
  }
    res.status(200).json(list);
  });
};

// Get car data by id
exports.getCar = function(req, res) {
  Car.findById(req.params.id, function(err, carInfo) {
  if (err) {
    res.status(500).send(err);
  }
    res.status(200).json(carInfo);
  });
};

