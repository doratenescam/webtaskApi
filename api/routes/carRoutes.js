'use strict';

module.exports = function(app) {
    var cars = require('../controllers/carController');

    //List cars
    app
    .route("/cars")
    .get(cars.listAllCars);


    //Get car by id
    app.route('/cars/:id')
    .get(cars.getCar);

};
